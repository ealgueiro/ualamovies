package com.example.springboot.service;


import java.util.List;

import com.example.springboot.model.Contenido;
import com.example.springboot.model.Usuario;

public interface UalaMoviesService {
	
	boolean usuarioMiraAlgoInteresante(Usuario user);
	
	void cobrarUsuario(Usuario user);
	
	boolean loginUsuario(Usuario user);
	
	boolean logoutUsuario(Usuario user);
	
	List<Contenido> getRecomendacionesUsuario(Usuario user);
	
}
