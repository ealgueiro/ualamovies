package com.example.springboot.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.example.springboot.model.Usuario;



@Service("userService")
public class UsuarioServiceImpl implements UsuarioService{
	
	private static final AtomicLong counter = new AtomicLong();
	
	private static List<Usuario> users;
	
	static{
		users= populateDummyUsers();
	}

	public List<Usuario> findAllUsers() {
		return users;
	}
	
	public Usuario findById(long id) {
		for(Usuario user : users){
			if(user.getId() == id){
				return user;
			}
		}
		return null;
	}
	
	public Usuario findByName(String name) {
		for(Usuario user : users){
			if(user.getName().equalsIgnoreCase(name)){
				return user;
			}
		}
		return null;
	}
	
	public void saveUser(Usuario user) {
		user.setId(counter.incrementAndGet());
		users.add(user);
	}

	public void updateUser(Usuario user) {
		int index = users.indexOf(user);
		users.set(index, user);
	}

	public void deleteUserById(long id) {
		
		for (Iterator<Usuario> iterator = users.iterator(); iterator.hasNext(); ) {
		    Usuario user = iterator.next();
		    if (user.getId() == id) {
		        iterator.remove();
		    }
		}
	}

	public boolean isUserExist(Usuario user) {
		return findByName(user.getName())!=null;
	}
	
	public void deleteAllUsers(){
		users.clear();
	}

	private static List<Usuario> populateDummyUsers(){
		List<Usuario> users = new ArrayList<Usuario>();
		users.add(new Usuario(counter.incrementAndGet(),"Sam", new Date()));
		users.add(new Usuario(counter.incrementAndGet(),"Tom", new Date()));
		users.add(new Usuario(counter.incrementAndGet(),"Jerome", new Date()));
		users.add(new Usuario(counter.incrementAndGet(),"Silvia", new Date()));
		return users;
	}

}
