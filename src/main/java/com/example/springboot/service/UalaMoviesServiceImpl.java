package com.example.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springboot.model.CanalComunicacion;
import com.example.springboot.model.Cobrador;
import com.example.springboot.model.Contenido;
import com.example.springboot.model.LoginManager;
import com.example.springboot.model.Recomendador;
import com.example.springboot.model.Usuario;



@Service("ualaMoviesService")
public class UalaMoviesServiceImpl implements UalaMoviesService {
	
	@Autowired
	Cobrador cobrador;
	
	@Autowired
	LoginManager loginManager;
	
	@Autowired
	Recomendador recomendador;

	@Override
	public boolean usuarioMiraAlgoInteresante(Usuario user) {
		return user.miraAlgoInteresante();
	}

	@Override
	public void cobrarUsuario(Usuario user) {
		float monto = cobrador.cobrar(user);
		for (CanalComunicacion canalComunicacion : user.getCanalesComunicacion()){
			canalComunicacion.notificarCobro(monto);
		}
	}

	@Override
	public boolean loginUsuario(Usuario user) {
		return loginManager.login(user);
	}

	@Override
	public boolean logoutUsuario(Usuario user) {
		return loginManager.logout(user);
	}

	@Override
	public List<Contenido> getRecomendacionesUsuario(Usuario user) {
		return recomendador.recomendar(user);
	}

}
