package com.example.springboot.service;


import java.util.List;

import com.example.springboot.model.Usuario;

public interface UsuarioService {
	
	Usuario findById(long id);
	
	Usuario findByName(String name);
	
	void saveUser(Usuario user);
	
	void updateUser(Usuario user);
	
	void deleteUserById(long id);

	List<Usuario> findAllUsers();
	
	void deleteAllUsers();
	
	boolean isUserExist(Usuario user);
	
}
