package com.example.springboot.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Usuario {

	private long id;

	private String name;

	private List<Contenido> viendoActualmente;

	private Date fechaRegistro;

	private MedioPago medioPago;

	private List<CanalComunicacion> canalesComunicacion;

	private Suscripcion suscripcion;

	private EstadoAnimo estadoAnimo;

	public Usuario() {
		id = 0;
	}

	public Usuario(long id, String name, Date fechaRegistro) {
		this.id = id;
		this.name = name;
		this.viendoActualmente = new ArrayList<Contenido>();
		this.fechaRegistro = fechaRegistro;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Contenido> getViendoActualmente() {
		return viendoActualmente;
	}

	public void setViendoActualmente(List<Contenido> viendoActualmente) {
		this.viendoActualmente = viendoActualmente;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public MedioPago getMedioPago() {
		return medioPago;
	}

	public void setMedioPago(MedioPago medioPago) {
		this.medioPago = medioPago;
	}

	public List<CanalComunicacion> getCanalesComunicacion() {
		return canalesComunicacion;
	}

	public void setCanalesComunicacion(List<CanalComunicacion> canalesComunicacion) {
		this.canalesComunicacion = canalesComunicacion;
	}

	public Suscripcion getSuscripcion() {
		return suscripcion;
	}

	public void setSuscripcion(Suscripcion suscripcion) {
		this.suscripcion = suscripcion;
	}

	public EstadoAnimo getEstadoAnimo() {
		return estadoAnimo;
	}

	public void setEstadoAnimo(EstadoAnimo estadoAnimo) {
		this.estadoAnimo = estadoAnimo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + "]";
	}

	public void agregarCanalComunicacion(CanalComunicacion canalComunicacion) {
		this.canalesComunicacion.add(canalComunicacion);
	}

	public void eliminarCanalComunicacion(CanalComunicacion canalComunicacion) {
		this.canalesComunicacion.remove(canalComunicacion);
	}
	
	public boolean miraAlgoInteresante(){
		boolean respuesta = false;
		for (Contenido contenido : viendoActualmente) {
			if (contenido.esInteresante()){
				respuesta = true;
				break;
			}
		}
		return respuesta;
	}

}
