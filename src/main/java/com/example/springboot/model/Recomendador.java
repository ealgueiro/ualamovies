package com.example.springboot.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Recomendador {

	private List<Contenido> contenidos;

	public List<Contenido> recomendar(Usuario usuario) {
		List<Contenido> respuesta = new ArrayList<Contenido>();

		EstadoAnimo estadoAnimo = usuario.getEstadoAnimo();

		for (Contenido contenido : contenidos) {
			if (estadoAnimo instanceof Triste) {
				if (contenido.getDuracion() > 2)
					respuesta.add(contenido);
			}
			if (estadoAnimo instanceof Contento) {
				respuesta.add(contenido);
			}
			if (estadoAnimo instanceof Melancolico) {
				if (contenido.getAñoEstreno() < Calendar.getInstance().get(Calendar.YEAR) - 10)
					respuesta.add(contenido);
			}
		}

		return respuesta;
	}

	public List<Contenido> getContenidos() {
		return contenidos;
	}

	public void setContenidos(List<Contenido> contenidos) {
		this.contenidos = contenidos;
	}

}
