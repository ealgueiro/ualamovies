package com.example.springboot.model;

public class Platinum extends Suscripcion {
	
	private float costo  = 300;

	public float getCosto() {
		return costo;
	}

	public void setCosto(float costo) {
		this.costo = costo;
	}
	
}
