package com.example.springboot.model;

import java.util.List;

public abstract class Suscripcion {
	
	protected float costo;
	
	protected List<Contenido> contenidos;

	public float getCosto() {
		return costo;
	}

	public void setCosto(float costo) {
		this.costo = costo;
	}

	public List<Contenido> getContenidos() {
		return contenidos;
	}

	public void setContenidos(List<Contenido> contenidos) {
		this.contenidos = contenidos;
	}
	
	
	
}
