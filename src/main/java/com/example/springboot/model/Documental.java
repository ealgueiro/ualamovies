package com.example.springboot.model;

public class Documental extends Contenido {
	
	@Override
	public boolean esInteresante() {
		return this.getTitulo().contains("unofficial");
	}

}
