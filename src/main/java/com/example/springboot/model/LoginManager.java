package com.example.springboot.model;

import java.util.HashMap;
import java.util.Map;

public class LoginManager {

	private Map<Usuario, Integer> personasUsuarioViendo = new HashMap<Usuario, Integer>();
	
	public boolean login(Usuario usuario){
		boolean success = false;
		
		Integer cantPersonas = this.personasUsuarioViendo.get(usuario);
		if (cantPersonas == null){
			cantPersonas = new Integer(1);
			this.personasUsuarioViendo.put(usuario, cantPersonas);
			success = true;
		} else {
			if (usuario.getSuscripcion() instanceof Classic){
				if (cantPersonas >= 1) {
					success = false;
				} else {
					cantPersonas = 1;
					success = true;
				}
			}
			
			if (usuario.getSuscripcion() instanceof Gold){
				if (cantPersonas >= 3) {
					success = false;
				} else {
					cantPersonas += 1;
					this.personasUsuarioViendo.put(usuario, cantPersonas);
					success = true;
				}
			}

			if (usuario.getSuscripcion() instanceof Classic){
				if (cantPersonas >= 5) {
					success = false;
				} else {
					cantPersonas += 1;
					this.personasUsuarioViendo.put(usuario, cantPersonas);
					success = true;
				}
			}

		}
		
		return success;
	}
	
	public boolean logout(Usuario usuario){
		boolean success = false;
		
		Integer cantPersonas = this.personasUsuarioViendo.get(usuario);
		if (cantPersonas == null){
			cantPersonas = new Integer(0);
			this.personasUsuarioViendo.put(usuario, cantPersonas);
			success = true;
		} else {
			cantPersonas -= 1;
			this.personasUsuarioViendo.put(usuario, cantPersonas);
			success = true;
		}
		
		return success;
	}
}