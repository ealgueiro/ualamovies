package com.example.springboot.model;

public class Pelicula extends Contenido {
	
	private int cantOscares;
	
	@Override
	public boolean esInteresante() {
		return cantOscares > 0;
	}

}
