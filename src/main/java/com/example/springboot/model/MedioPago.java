package com.example.springboot.model;

public abstract class MedioPago {
	
	protected float porcentajeDescuento;
	
	public float aplicarDescuento(float monto) {
		return monto - ((monto * porcentajeDescuento) / 100);
	}

	public float getPorcentajeDescuento() {
		return porcentajeDescuento;
	}

	public void setPorcentajeDescuento(float porcentajeDescuento) {
		this.porcentajeDescuento = porcentajeDescuento;
	};
	
}
