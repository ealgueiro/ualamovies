package com.example.springboot.model;

public class Serie extends Contenido {
	
	private int cantTemporadas;
	
	@Override
	public boolean esInteresante() {
		return cantTemporadas == 4 || cantTemporadas == 5;
	}

}
