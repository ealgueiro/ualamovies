package com.example.springboot.model;

import java.util.Calendar;
import java.util.Date;

public class Cobrador {

	public float cobrar (Usuario usuario) {
		
		float costoSuscripcion = usuario.getSuscripcion().getCosto();
		
		costoSuscripcion = usuario.getMedioPago().aplicarDescuento(costoSuscripcion);
		
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -1); // to get previous year add -1
		Date lastYear = cal.getTime();
		
		if (usuario.getFechaRegistro().before(lastYear)){
			costoSuscripcion = costoSuscripcion - ((costoSuscripcion * 3) / 100);
		}
		
		return costoSuscripcion;
	}
}
